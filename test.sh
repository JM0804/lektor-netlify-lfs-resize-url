#!/bin/sh

python -m mypy lektor_netlify_lfs_resize_url.py
python -m mypy test.py
python -m flake8 lektor_netlify_lfs_resize_url.py
python -m flake8 test.py
python -m unittest test.py
python -m bandit -q lektor_netlify_lfs_resize_url.py
