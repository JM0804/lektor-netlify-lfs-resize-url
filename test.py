import unittest
from lektor_netlify_lfs_resize_url import nf_resize

SOURCE = 'file.jpg'
H = 1
W = 1


class ArgsTestCase(unittest.TestCase):
    def test_no_args(self):
        result = nf_resize(SOURCE)
        self.assertEqual(result, SOURCE)
        # self.assertRaises(Exception, nf_resize(SOURCE))

    def test_wrong_nf_resize(self):
        self.assertRaises(Exception, nf_resize, SOURCE, nf_resize='wrong!')

    def test_h_no_w(self):
        result = nf_resize(SOURCE, h=H)
        expected = f'{SOURCE}?nf_resize=fit&h={H}'
        self.assertEqual(result, expected)

    def test_w_no_h(self):
        result = nf_resize(SOURCE, w=W)
        expected = f'{SOURCE}?nf_resize=fit&w={W}'
        self.assertEqual(result, expected)

    def test_h_and_w(self):
        result = nf_resize(SOURCE, h=H, w=W)
        expected = f'{SOURCE}?nf_resize=fit&h={H}&w={W}'
        self.assertEqual(result, expected)

    def test_fit_no_h_or_w(self):
        result = nf_resize(SOURCE, nf_resize='fit')
        self.assertEqual(result, SOURCE)

    def test_fit_h_no_w(self):
        result = nf_resize(SOURCE, nf_resize='fit', h=H)
        expected = f'{SOURCE}?nf_resize=fit&h={H}'
        self.assertEqual(result, expected)

    def test_fit_w_no_h(self):
        result = nf_resize(SOURCE, nf_resize='fit', w=W)
        expected = f'{SOURCE}?nf_resize=fit&w={W}'
        self.assertEqual(result, expected)

    def test_fit_h_and_w(self):
        result = nf_resize(SOURCE, nf_resize='fit', h=H, w=W)
        expected = f'{SOURCE}?nf_resize=fit&h={H}&w={W}'
        self.assertEqual(result, expected)

    def test_smartcrop_no_h_or_w(self):
        result = nf_resize(SOURCE, nf_resize='smartcrop')
        self.assertEqual(result, SOURCE)

    def test_smartcrop_h_no_w(self):
        result = nf_resize(SOURCE, nf_resize='smartcrop', h=H)
        expected = f'{SOURCE}?nf_resize=smartcrop&h={H}'
        self.assertEqual(result, expected)

    def test_smartcrop_w_no_h(self):
        result = nf_resize(SOURCE, nf_resize='smartcrop', w=W)
        expected = f'{SOURCE}?nf_resize=smartcrop&w={W}'
        self.assertEqual(result, expected)

    def test_smartcrop_h_and_w(self):
        result = nf_resize(SOURCE, nf_resize='smartcrop', h=H, w=W)
        expected = f'{SOURCE}?nf_resize=smartcrop&h={H}&w={W}'
        self.assertEqual(result, expected)


if __name__ == "__main__":
    unittest.main()
